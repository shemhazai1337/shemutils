import os
import subprocess
import time
import logging
import random
import hashlib
import getpass
import sys
import struct
import rsa
import multiprocessing
from Crypto.Cipher import AES


class bcolors:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'


def green(message):
    return bcolors.OKGREEN + message + bcolors.ENDC


def blue(message):
    return bcolors.OKBLUE + message + bcolors.ENDC


def yellow(message):
    return bcolors.WARNING + message + bcolors.ENDC


def red(message):
    return bcolors.FAIL + message + bcolors.ENDC


def bold(message):
    return bcolors.BOLD + message + bcolors.ENDC


def underline(message):
    return bcolors.UNDERLINE + message + bcolors.ENDC


def importationerror(module):
    print "ShemUtils Error: You need to install %s module to use this library." % module
    return


class Logger:
    def __init__(self, logger_name):
        """Logging module wrapper to easily encapsulate code for re-use in my own projects."""
        self.logger_name = logger_name
        self.operational_system = os.name
        self.color_flag = self.set_color_flag()
        self.loggerHandle = self._create_logger()
        self.consoleHandle = self._create_console()
        self.formatter = self._create_formatter()
        self.define_logger_level([self.loggerHandle, self.consoleHandle])
        self.consoleHandle.setFormatter(self.formatter)
        self.loggerHandle.addHandler(self.consoleHandle)

    def set_color_flag(self):
        return True if self.operational_system == "posix" else False

    def define_logger_level(self, objs):
        return [self._set_level(x) for x in objs]

    def _create_logger(self):
        return logging.getLogger(self.logger_name)

    @staticmethod
    def _create_console():
        return logging.StreamHandler()

    @staticmethod
    def _create_formatter():
        return logging.Formatter("%(asctime)s [%(name)s] %(levelname)s: %(message)s", "%H:%M:%S")

    @staticmethod
    def _set_level(logger_obj, level=logging.DEBUG):
        return logger_obj.setLevel(level)

    def success(self, string):
        if self.color_flag:
            self.loggerHandle.info(green("[^] ") + string)
        else:
            self.loggerHandle.info(string)
        return

    def step_ok(self, string):
        max_len = 32
        diff = max_len - len(string)
        if self.color_flag:
            self.loggerHandle.info("{0} ".format(string) + " ".rjust(diff, ".") + ": " + green("SUCCESS"))
        else:
            self.loggerHandle.info("{0} ".format(string) + " ".rjust(diff, ".") + ": " + "SUCCESS")

    def step_fail(self, string):
        max_len = 32
        diff = max_len - len(string)

        if self.color_flag:
            self.loggerHandle.info("{0} ".format(string) + " ".rjust(diff, ".") + ": " + red("FAILED"))
        else:
            self.loggerHandle.info("{0} ".format(string) + " ".rjust(diff, ".") + ": " + "FAILED")

    def info(self, string):
        if self.color_flag:
            self.loggerHandle.info(blue("[*] ") + string)
        else:
            self.loggerHandle.info(string)
        return

    def debug(self, string):
        if self.color_flag:
            self.loggerHandle.debug(yellow("[#] ") + string)
        else:
            self.loggerHandle.debug(string)
        return

    def warning(self, string):
        if self.color_flag:
            self.loggerHandle.warning(yellow("[!] ") + string)
        else:
            self.loggerHandle.warning(string)
        return

    def error(self, string):
        if self.color_flag:
            self.loggerHandle.error(red("[@] ") + string)
        else:
            self.loggerHandle.error(string)
        return

    def critical(self, string):
        if self.color_flag:
            self.loggerHandle.critical(red("[!!] ") + string)
        else:
            self.loggerHandle.critical(string)
        return


class Encryption:
    """This module uses pycrypto for encryption"""

    @staticmethod
    def create_iv():
        return ''.join(chr(random.randint(0, 0xFF)) for i in range(16))

    @staticmethod
    def hash256(string):
        return hashlib.sha256(string).digest()

    @staticmethod
    def hash512(string):
        return hashlib.sha512(string).digest()

    @staticmethod
    def hashmd5(string):
        return hashlib.md5(string).digest()

    @staticmethod
    def get_key(bits=256):
        sys.stderr.write("Bit-size selected: %d\n" % bits)
        k = str()
        c = str("c")
        while k != c:
            if k == c:
                break
            k = getpass.getpass("Type your key: ", sys.stderr)
            c = getpass.getpass("Confirm your key: ", sys.stderr)
        if bits == 256:
            sys.stderr.write("Generating 256-bit key ...\n")
            return Encryption.hash256(k)
        elif bits == 128:
            sys.stderr.write("Generating 128-bit key ...\n")
            return Encryption.hashmd5(k)

    @staticmethod
    def encrypt_file(file_name, key, iv, output=None, chunksize=64*1024):
        if not output:
            dire_name = os.path.dirname(file_name)
            base_name = os.path.basename(file_name)
            if dire_name != "":
                output = dire_name + os.sep + base_name + ".enc"
            else:
                output = base_name + ".enc"
        filesize = os.path.getsize(file_name)
        encryptor = AES.new(key, AES.MODE_CBC, iv)
        with open(file_name, "rb") as infile:
            with open(output, "wb") as outfile:
                outfile.write(struct.pack("<Q", filesize))
                outfile.write(iv)
                while True:
                    chunk = infile.read(chunksize)
                    if len(chunk) == 0:
                        break
                    elif len(chunk) % 16 != 0:
                        chunk += " " * (16 - len(chunk) % 16)
                    outfile.write(encryptor.encrypt(chunk))
        return True

    @staticmethod
    def decrypt_file(file_name, key, output=None, chunksize=64*1024):
        if not output:
            root_name, ext = os.path.splitext(file_name)
            if ext != ".enc":
                return False
            dir_name = os.path.dirname(root_name) + os.sep
            f_name = os.path.basename(root_name)
            if dir_name != "/":
                output = dir_name + f_name
            else:
                output = f_name
        with open(file_name, "rb") as infile:
            origsize = struct.unpack("<Q", infile.read(struct.calcsize('Q')))[0]
            iv = infile.read(16)
            decryptor = AES.new(key, AES.MODE_CBC, iv)
            with open(output, "wb") as outfile:
                while True:
                    chunk = infile.read(chunksize)
                    if len(chunk) == 0:
                        break
                    outfile.write(decryptor.decrypt(chunk))
                outfile.truncate(origsize)
        return True

    @staticmethod
    def get_chunk(msg, index, chunksize=16):
        return msg[index:index+chunksize]

    @staticmethod
    def split_string(string, chunksize=16):
        output = []
        for x in range(0, len(string), chunksize):
            part = Encryption.get_chunk(string, x, chunksize=chunksize)
            if len(part) % 16 != 0:
                part += " " * (16 - len(part) % 16)
            output.append(part)
        return output

    @staticmethod
    def encrypt_message(plaintext, key, iv):
        psize = sys.getsizeof(plaintext)
        encryptor = AES.new(key, AES.MODE_CBC, iv)
        cipher = str()
        cipher += struct.pack("<Q", psize)
        cipher += iv
        for chunk in Encryption.split_string(plaintext):
            cipher += encryptor.encrypt(chunk)
        return cipher

    @staticmethod
    def decrypt_message(cipher, key):
        iv = cipher[struct.calcsize("Q"):struct.calcsize("3Q")]
        decryptor = AES.new(key, AES.MODE_CBC, iv)
        plaintext = str()
        for chunk in Encryption.split_string(cipher[struct.calcsize("3Q"):]):
            plaintext += decryptor.decrypt(chunk)
        return plaintext


class RSA:
    """This class have dependencies.
    multiprocessing, rsa modules are needed.
    """
    def __init__(self):
        self.logger = Logger("RSA")
        self.public_key = None
        self.private_key = None
        self.cpu_count = multiprocessing.cpu_count()

    def generate_keypair(self, bits=4096):
        self.logger.info("Generating new %d-bits key pair ..." % bits)
        self.public_key, self.private_key = rsa.newkeys(bits, poolsize=self.cpu_count)
        return True

    def encrypt_message(self, message):
        self.logger.info("Encrypting message ...")
        s1 = time.time()
        crypto = rsa.encrypt(message, self.public_key)
        s2 = time.time()
        self.logger.info("Encryption success.")
        self.logger.info("Procedure took %d seconds." % (s2 - s1))
        return crypto

    def decrypt_message(self, cipher):
        self.logger.info("Decrypting cipher ...")
        s1 = time.time()
        decrypto = rsa.decrypt(cipher, self.private_key)
        s2 = time.time()
        self.logger.info("Decryption success.")
        self.logger.info("Procedure took %d seconds." % (s2 - s1))
        return decrypto

    def save_keys(self, priv_f="private_key.pem",  pub_f="public_key.pem"):
        if self.public_key is None:
            self.logger.error("Public Key does not exists. Generate it.")
            return False
        if self.private_key is None:
            self.logger.error("Private Key does not exists. Generate it.")
            return False
        with open(priv_f, "w") as priv:
            priv.write(self.private_key.save_pkcs1())
            self.logger.info("Private key saved to file '%s'" % priv_f)
        with open(pub_f, "w") as pub:
            pub.write(self.public_key.save_pkcs1())
            self.logger.info("Public key saved to file '%s" % pub_f)
        return True

    def load_keys(self, priv_f, pub_f):
        if not os.path.isfile(priv_f):
            self.logger.error("Private key file does not exists.")
        if not os.path.isfile(pub_f):
            self.logger.error("Public key file does not exists.")
        with open(priv_f, "rb") as priv:
            priv_data = priv.read()
            priv_data = priv_data.encode("ascii")
        with open(pub_f, "rb") as pub:
            pub_data = pub.read()
            pub_data = pub_data.encode("ascii")
        self.private_key = rsa.PrivateKey.load_pkcs1(priv_data)
        self.public_key = rsa.PublicKey.load_pkcs1(pub_data)
        self.logger.info("Key pair successfully loaded.")
        return True


class FileExists(Exception):
    pass


class EmptyBuffer(Exception):
    pass


class MD5Sum(object):
    def __init__(self, file_object):
        self.file = file_object
        self.file_fd = int()
        self.buffer = str()
        self.md5_checksum = str()

    def start(self):
        """
        Establish logical order from object methods
        """
        if not self._check_exists():
            raise FileExists("File '{0}' does not exists.".format(self.file))

        self.file_fd = self._get_fd()  # returns a fd to file_fd var
        self.buffer = self._read_bytes(512)  # reads 512 bytes from file

        if not self.buffer or len(self.buffer) == 0:
            raise EmptyBuffer("Empty buffer")

        self.md5_checksum = self.retrieve_md5()
        if not self.md5_checksum or len(self.buffer) == 0:
            raise EmptyBuffer("Empty MD5 buffer")

    def retrieve_md5(self):
        m = hashlib.md5()
        m.update(self.buffer)
        return m.hexdigest()

    def _read_bytes(self, n):
        return self.file_fd.read(n) if type(self.file_fd) is not int else ""

    def _get_fd(self):
        return open(self.file, "rb")

    def _check_exists(self):
        return True if os.path.exists(self.file) else False
